"set	cindent
set	smartindent
"set	autoindent
set	nowrap
set	ff=unix
set	bg=dark
set	ruler
set	path=/root/work/include,/usr/include,/usr/local/include,/usr/src/include,./
syntax	on
"set	fenc=utf-8
"set	enc=utf-8
set	ts=2
set nu
filetype plugin on

"== for vundle ==
set nocompatible               " be iMproved
"filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required!
Bundle 'gmarik/vundle'

" bundles
Bundle 'The-NERD-tree'
"Bundle 'SuperTab-continued.'
Bundle 'EnhCommentify.vim'
Bundle 'terryma/vim-multiple-cursors'

"============key 매핑=================
map <F2> v]}zf			"폴딩
map <F3> zo			"폴딩해제
map <F4> :Tlist<cr><C-W><C-W>	"taglist
map <F5> :NERDTree<cr>	"NERDTree
map <F6> :CompView<cr>
map <PageUp> <C-U><C-U>
map <PageDown> <C-D><C-D>

map <Right> l
map <Left> h
map <Down> j
map <Up> k

map <C-F> l
map <C-B> h
map <C-P> k
map <C-N> j
map <C-E> $
map <C-A> ^

imap <C-F> <Right>
imap <C-B> <Left>
imap <C-N> <Down>
imap <C-P> <Up>
imap <C-A> <Home>
imap <C-E> <End>

"== for multiple cursors ==
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_next_key='<C-d>'
let g:multi_cursor_quit_key='<Esc>'

"============ 인덴트 설정 및 제거 =====
map ,noi :set noai<CR>:set nocindent<CR>:set nosmartindent<CR>
map ,sei :set ai<CR>:set cindent<CR>:set smartindent<CR>


"================파일 버퍼간 이동======
map	,1 :b!1<CR>
map	,2 :b!2<CR>
map	,3 :b!3<CR>
map	,4 :b!4<CR>
map	,5 :b!5<CR>
map	,6 :b!6<CR>
map	,7 :b!7<CR>
map	,8 :b!8<CR>
map	,9 :b!9<CR>
map	,0 :b!0<CR>
map	,w :bw<CR>
map	<F10> <ESC>i/*************************************************************************** <NL>File        : <NL>Date        : <NL>Author      : <NL>Modified    :      <NL>Description : <NL>***************************************************************************/<NL><ESC><CR>
map	<F9> <ESC>i/*************************************************************************** <NL>Function    : <NL>Date        : <NL>Author      : <NL>Parameter   : <NL>Return value:<NL>Description : <NL>***************************************************************************/<NL><ESC><CR>
map	<F8> <ESC>i#ifndef <NL>#define<NL><NL>#endif<SPACE>//<ESC><CR>
map	<F7> <ESC>a2007/06/05<DOWN>Sun Joon Gyu<ESC>
map	,co <ESC><END>a<TAB>/* */<ESC>hhi
map	,fo <ESC>i<NL>for( ; ; ){<NL><NL>}<ESC>kkllll
map	,if <ESC>i<NL>if( ){<NL><NL>}<ESC>kklll
map	,ef <ESC>aelse if( ){<NL><NL>}<ESC>kkllllllll
map	,es <ESC>aelse{<NL><NL>}<ESC>k<TAB>
map	,in <ESC>i<HOME>#include<><ESC>i
map	,de <ESC>i<HOME>#define<TAB>
map	,li :TaskList<CR>


" Commenting blocks of code.
autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType tex              let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

"================ctags 설정 ===========
set tags=tags,../tags,/usr/include/tags,/usr/local/include/tags

if version >= 500
func! Sts()
	let st = expand("<cword>")
	exe "sts ".st
endfunc
nmap ,st :call Sts()<cr>

func! Tj()
	let st = expand("<cword>")
	exe "tj ".st
endfunc
nmap ,tj :call Tj()<cr>

endif

"=============== cscope 설정 =========
set csprg=/usr/bin/cscope
set csto=0
set cst 
set nocsverb

if filereadable("./cscope.out")
	cs add cscope.out
	cs add ./cscope.out
else
	cs add ./cscope.out
endif
set csverb

func! Css()
	let css = expand("<cword>")
	new
	exe "cs find s ". css
	if getline(1) == " "
		exe "q!"
	endif
endfunc
nmap ,css :call Css()<cr>

func! Csc()
	let csc = expand("<cword>")
	new
	exe "cs find c ".csc
	if getline(1) == " "
		exe "q!"
	endif
endfunc
nmap ,csc :call Csc()<cr>

"=============== man page setup ==========
func! Man()
	let sm = expand("<cword>")
	exe "!man -S 2:3:4:5:6:7:8:9:tcl:n:1:p:o ".sm
endfunc
nmap ,ma :call Man()<cr><cr>

"======================인터넷 사전설정=====
func! Engdic()
	let ske = expand("<cword>")
	new 
	exec "r !lynx -nolist - verbose - dump http://kr.engdic.yahoo.com/result.html?p=".ske " | sed 's/\\[ INLINE\\]//g' | tail +23"
	set nomod wrap
	noremap <buffer> q : bd<cr> : echo ' '<cr>
endfunc
nmap ,ed : call Engdic()<cr>gg


